var path = require("path");

module.exports = {
	context: __dirname,
  entry: './main.js',
	module: {
		loaders: [
			{ test: /\.less$/, loader: "style!css!less" },
			{ test: /\.css$/, loader: "style!css" },
			{ test: /\.png$/, loader: "file" }
		]
	}
};

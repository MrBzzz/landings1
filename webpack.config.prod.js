var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	context: __dirname,
  entry: './main.js',
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
		loaders: [
			{ test: /\.less$/, loader: ExtractTextPlugin.extract('style', 'css!less') },
			{ test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css') }
		]
	},
  plugins: [
    new ExtractTextPlugin('styles.css', {
      allChunks: true
    })
  ]
};
